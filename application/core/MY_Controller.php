<?php
class MY_Controller extends CI_Controller{
    private $email_initialized = false;
    const FORM_HASH = 'k30_123d01ad';
    const SUCCESS   = 'success';
    /**
    * @var Account_model
    */
    public $account_model;
    /**
    * @var Api_papercut
    */
    public $api_papercut;
    /**
    * @var Api_firstdata
    */    
    public $api_firstdata;
    /**
    * @var Marshaller
    */
    public $marshaller;
    
    
    protected $user;
        
    public function __construct(){
        parent::__construct();
        
        $this->load->database();
        
        $this->load->library('api_firstdata');
        $this->load->library('api_papercut');
        //user marshalling
        $this->load->library('marshaller');
        //for easier creation of bootstrap forms
        $this->load->library('bootstrap_form_helper');
        
        //$this->load->library('my_session', array(), 'session');
        
        $this->load->model('account_model');
        $this->load->config('portal_config');
        
        $this->load->helper('form');
        $this->load->helper('url');
        //Papercut API configuration
        $this->api_papercut->initialize(
            $this->config->item('papercut_url'),
            $this->config->item('papercut_token')
        );
        //FirstaData API configuration
        $this->api_firstdata->initialize(
            $this->config->item('payment_wsdl'),
            $this->config->item('payment_gateway_hmac'),
            $this->config->item('payment_key_id'),
            $this->config->item('payment_gateway_id'),
            $this->config->item('payment_gateway_password')
        );
        
        $this->marshaller->initialize(
            $this->uri,
            $this->input
        );
    }
    
    protected function _generate_token(){
        $token_salt =  time().rand(1,20000);
        $token = md5(self::FORM_HASH.$token_salt);
        //$this->session->set_userdata('token_salt', $token_salt);
        
        return $token;
    }
    
    protected function _validate_token($token){
        $token_salt = $this->session->userdata('token_salt');
        $valid      = false;
        if( md5(self::FORM_HASH.$token_salt ) == $token ){
            $old_token = $this->session->userdata('token');
            if( $old_token ){
                if( $token != $old_token ){
                    $valid = true;               
                } else {
                    $valid = false;
                }
            } else {
                $valid = true;
            }
            //$this->session->set_userdata('token', $token );
        }
        
        if( !$valid ){
            //throw new AccountFormResubmisionException('Cant resubmit same transaction');
        }
        return $valid;
    }
    
    protected function _generate_user_token(user $user){
        $token = md5(self::FORM_HASH.$user->username.$user->papercut_id);
        return $token;
    }
    
    protected function _validate_user_token( $token, user $user ){
        if($token != $this->_generate_user_token($user)){
            throw new InvalidTokenException($user,'Invalid token');
        }        
    }
                
    protected function _set_user(user $user){
        $this->session->set_userdata(
            array(
                'username'=> $user->username,
                'pass'=>     $user->password,
                'id'=>       $user->id,
                'pid'=>      $user->papercut_id,  
            )
        );
    }
    
    protected function _get_user( user $passed_user = null){
        $user = new user();
        if( $username = $this->session->userdata('username')){
            $user->username = $username;
        }  
        
        if( $this->session->userdata('pass') ){
            $user->password = $this->session->userdata('pass'); 
        }
        
        if( $this->session->userdata('id')){
            $user->id = $this->session->userdata('id');
        }
        
        if( $this->session->userdata('id')){
            $user->papercut_id = $this->session->userdata('pid');
        }
                
        if( !$user->username || !$user->password || !$user->id ){
            throw new PaymentPortalException($user);
        }
        
        if( $passed_user && !$user->same_as($passed_user)){
            throw new PaymentPortalException($user);    
        }
        
        $user->is_logged = true;
        $user->is_registered = true;
        
        return $user;
    }
    
    protected function _redirect_to_login_page( $user = null){
        header('Location: '.site_url('login'));
    }
    
    protected function _redirect_to_papercut(){
        header('Location: '.$this->config->item('papercut_backoffice'));
    }
    
    protected function is_user_logged( user $user ){
        if( $this->session->userdata('username') == $user->username &&  $this->session->userdata('pass')){
            return true;
        }
        return false;
    }
    
    protected function get_user_pass(){
        return $this->session->userdata('pass');
    }
    
    protected function _send_email_receipt(user $user){
        if(!$this->email_initialized) {
            $this->initialize_email();
        }
        $this->email->from( $this->config->item('payment_from_address'), 'MicroOffice Payment Portal');
        $this->email->to( $user->email );
        $this->email->subject('MicroOffice Payment Portal Creditcard Charge Notification: '.gmdate("m/d/Y"));
        $this->email->message($user->transaction_data->description);        
        $this->email->send();
    }
    
    private function initialize_email(){
        if( $this->email_initialized ) return;
        
        $this->load->library('email'); 
        $config['mailtype']  = 'html';
        $config['protocol']  = 'smtp';
        $config['smtp_host'] = $this->config->item('payment_smtp_host');
        $config['smtp_user'] = $this->config->item('payment_smtp_user');
        $config['smtp_pass'] = $this->config->item('payment_smtp_pass');
        $this->email->initialize( $config );
        $this->email_initialized = true;
                
    }
    protected function _authenticate(){
        try{
            if( !$this->user ){
                $this->user = $this->_get_user( $this->marshaller->marshall_papercut_user());    
            }
        } catch( PaymentPortalException $e ){
            $this->_redirect_to_login_page();
            die();
        }
        return $this->user;
    }
    
    protected function _initialize_user(){
        $user = $this->_authenticate();
        $user->balance = $this->api_papercut->get_user_balance($user);
        $user->is_cc_data_saved = $this->account_model->user_has_saved_cc_details($user);
        $user->has_recurring_payment = $this->account_model->user_has_recurring_payment_data($user);
        return $user;
    }
    
    protected function _load_cc_data( user $user ){
        $user = $this->_get_user($user);
        if( $this->account_model->user_has_saved_cc_details( $user )){
            $user = $this->account_model->load_cc_data($user);
            $user = $this->account_model->get_recurring_payment_data($user);                 
        }
        return $user;        
    }    
}

class  PaymentPortalException extends Exception{
    public $user;
    public function __construct( user $user, $message = ''){
        $this->user = $user;
        parent::__construct($message);
    }    
}

class InvalidTokenException extends PaymentPortalException{}
class AccountFormResubmisionException extends PaymentPortalException{}