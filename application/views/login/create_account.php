<?php

$fields = array(
    'first_name' => array(
        'label' => 'First Name',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => $user->first_name,            
        ),
        'error' => isset($user->errors['first_name']) ? true : false
    ),
    'last_name' => array(
        'label' => 'Last Name',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => $user->last_name        
        ),
        'error' => isset($user->errors['last_name']) ? true : false
    ),
    'email' => array(
        'label' => 'Email',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => $user->email,
            'type'  => 'email'
        ),
        'error' => isset($user->errors['email']) ? true : false
    ),    
    'password' => array(
        'label' => 'Password',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => '',
            'id'    => 'password',
            'type'  => 'password'
        ),
        'error' => isset($user->errors['password']) ? true : false
    ),
    'password2' => array(
        'label' => 'Retype Password',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => '',
            'id'    => 'password2',
            'type'  => 'password'
        ),
        'name_override' => 'password2',
        'error' => isset($user->errors['password']) ? true : false
    ),        
);
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>MicroOffice Payment Portal</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php /*$this->load->view(
            'common/partials/navbar', 
            array(
                'user'    => $user,
                'balance' => $balance,
                'papercut_url' => $papercut_url
            )
        );*/ ?>
        
        <div class="container">
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8"> 
                    <div id="status" class="<?php echo (isset($status) && $status ? ($status == MY_Controller::SUCCESS ? 'alert alert-success' : 'alert alert-danger') : ''  )?>">
                        <?php echo (isset($status) && $status ? ($status == MY_Controller::SUCCESS ? 'Account created successfully' : $status) : '') ?>
                    </div>
                </div>
                <div class="col-md-2"> </div>    
            </div>
            <div class="row">
                <div class="col-md-2"> </div>
                
                <div class="col-md-8">
                    <form role="form" action="<?php echo site_url('login/new_account')?>" method="post">
                    
                      <?php echo $bootstrap_form_helper->generate_form_elements($fields) ?>
                                                                                 
                      <button id="create"   class="btn btn-primary">Create</button>
                      <input  type="hidden" name="papercut_id" value="<?php echo $user->papercut_id ?>">
                      <input  type="hidden" name="username"    value="<?php echo $user->username ?>">
                      <input  type="hidden" name="token"       value="<?php echo $token ?>">
                    </form>
                </div>
               
                <div class="col-md-2"></div>
            </div>    
        </div>
   
        <script type="text/javascript">
        $(document).ready(
            function(){
                
                $('#create').click(                
                    function(){
                        $('.has-error').removeClass('has-error');
                        var valid = true;
                        $('[data-mandatory="true"]').each(
                            function(){
                                if( $(this).val() == '' ){
                                    $(this).parent().addClass('has-error');
                                    valid = false;
                                }
                            }
                        )
                        
                        return valid;
                    }
                )
                
                $('#password2').change(
                    function(){
                        $('#status').removeClass('alert alert-danger').text('');
                        if($(this).val()!= $('#password').val()){
                            $('#status').addClass('alert alert-danger').text('Passwords do not match');
                        }
                    }
                )
                
            }
        );
        </script>        
    </body>    
</html>

