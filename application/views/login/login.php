<!DOCTYPE html>
<html lang="en">

    <head>
        <title>MicroOffice Payment Portal</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php /*$this->load->view(
            'common/partials/navbar', 
            array(
                'user'    => $user,
                'balance' => $balance,
                'papercut_url' => $papercut_url
            )
        );*/ ?>
        
        <div class="container">
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8"> 
                <?php 
                if( isset($status) && $status ){
                    if( $status != MY_Controller::SUCCESS ){
                        $class = 'alert alert-danger';
                    }
                    echo '<div class="'.$class.'">'.$status.'</div>';
                }
                ?>
                </div>
                <div class="col-md-2"> </div>    
            </div>
            <div class="row">
                <div class="col-md-2"> </div>
                
                <div class="col-md-8">
                    <form role="form" action="<?php echo site_url('login/authenticate')?>" method="post">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" name="username" class="form-control"     data-mandatory="true" id="exampleInputEmail1" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" data-mandatory="true" id="exampleInputPassword1" placeholder="">
                      </div>                                       


                      <button id="login" class="btn btn-primary">Login</button>

                      <a href="<?php echo $papercut_url ?>" class="btn btn-default">Back to Papercut</a>
                      <input  type="hidden" name="token" value="<?php echo '$token' ?>">
                    </form>
                </div>
               
                <div class="col-md-2"></div>
            </div>    
        </div>
   
        <script type="text/javascript">
        $(document).ready(
            function(){
                $('#login').click(                
                    
                    function(){
                        $('.has-error').removeClass('has-error');
                        var valid = true;
                        $('[data-mandatory="true"]').each(
                            function(){
                                if( $(this).val() == '' ){
                                    $(this).parent().addClass('has-error');
                                    valid = false;
                                }
                            }
                        )
                        
                        return valid;
                    }
                )            
            }
        );
        </script>        
    </body>    
</html>
