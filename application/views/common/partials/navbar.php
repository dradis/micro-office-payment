<?php
  
?>
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">MicroOffice Payment Portal</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><p class="navbar-text"><?php echo "Welcome <b>$user->username</b>, your Papercut balance is <b>USD $user->balance</b>"?></p></li>
      </ul>
    
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo $papercut_url ?>">Back to Papercut</a></li>
      </ul>
      
      <?php if( $user->is_logged ) { ?>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url('login')?>">Sign Out</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url('settings/user/'.$user->username.'/'.$user->papercut_id)?>">Settings</a></li>
          </ul>       
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url('transactions/user/'.$user->username.'/'.$user->papercut_id)?>">Transactions</a></li>
          </ul>
      <?php } else { ?>
           <ul class="nav navbar-nav navbar-right">
            <li><a href="<?php echo site_url('login')?>">Login</a></li>
          </ul>     
      <?php } ?>
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo site_url('charge/user_papercut/'.$user->username.'/'.$user->papercut_id)?>">Charge</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo site_url('dashboard/user/'.$user->username.'/'.$user->papercut_id)?>">Dashboard</a></li>
      </ul>                 
            
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
