<?php
$states = array(
    ''   => '- Select State',
    "Alabama" => "Alabama",
    "Alaska" => "Alaska",
    "Arizona" => "Arizona",
    "Arkansas" => "Arkansas",
    "California" => "California",
    "Colorado" => "Colorado",
    "Connecticut" => "Connecticut",
    "Delaware" => "Delaware",
    "Florida" => "Florida",
    "Georgia" => "Georgia",
    "Hawaii" => "Hawaii",
    "Idaho" => "Idaho",
    "Illinois" => "Illinois",
    "Indiana" => "Indiana",
    "Iowa" => "Iowa",
    "Kansas" => "Kansas",
    "Kentucky" => "Kentucky",
    "Louisiana" => "Louisiana",
    "Maine" => "Maine",
    "Maryland" => "Maryland",
    "Massachusetts" => "Massachusetts",
    "Michigan" => "Michigan",
    "Minnesota" => "Minnesota",
    "Mississippi" => "Mississippi",
    "Missouri" => "Missouri",
    "Montana" => "Montana",
    "Nebraska" => "Nebraska",
    "Nevada" => "Nevada",
    "New Hampshire" => "New Hampshire",
    "New Jersey" => "New Jersey",
    "New Mexico" => "New Mexico",
    "New York" => "New York",
    "North Carolina" => "North Carolina",
    "North Dakota" => "North Dakota",
    "Ohio" => "Ohio",
    "Oklahoma" => "Oklahoma",
    "Oregon" => "Oregon",
    "Pennsylvania" => "Pennsylvania",
    "Rhode Island" => "Rhode Island",
    "South Carolina" => "South Carolina",
    "South Dakota" => "South Dakota",
    "Tennessee" => "Tennessee",
    "Texas" => "Texas",
    "Utah" => "Utah",
    "Vermont" => "Vermont",
    "Virginia" => "Virginia",
    "Washington" => "Washington",
    "West Virginia" => "West Virginia",
    "Wisconsin" => "Wisconsin",
    "Wyoming" => "Wyoming"
);

$countries = array(
    ''   => '- Select Country',
    'AF' => 'Afghanistan',
    'AX' => 'Aland Islands',
    'AL' => 'Albania',
    'DZ' => 'Algeria',
    'AS' => 'American Samoa',
    'AD' => 'Andorra',
    'AO' => 'Angola',
    'AI' => 'Anguilla',
    'AG' => 'Antigua and Barbuda',
    'AR' => 'Argentina',
    'AM' => 'Armenia',
    'AW' => 'Aruba',
    'AU' => 'Australia',
    'AT' => 'Austria',
    'AZ' => 'Azerbaijan',
    'BS' => 'Bahamas',
    'BH' => 'Bahrain',
    'BD' => 'Bangladesh',
    'BB' => 'Barbados',
    'BY' => 'Belarus',
    'BE' => 'Belgium',
    'BZ' => 'Belize',
    'BJ' => 'Benin',
    'BM' => 'Bermuda',
    'BT' => 'Bhutan',
    'BO' => 'Bolivia',
    'BA' => 'Bosnia and Herzegovina',
    'BW' => 'Botswana',
    'BR' => 'Brazil',
    'BN' => 'Brunei Darussalam',
    'BG' => 'Bulgaria',
    'BF' => 'Burkina Faso',
    'BI' => 'Burundi',
    'KH' => 'Cambodia',
    'CM' => 'Cameroon',
    'CA' => 'Canada',
    'CV' => 'Cape Verde',
    'KY' => 'Cayman Islands',
    'CF' => 'Central African Republic',
    'TD' => 'Chad',
    'CL' => 'Chile',
    'CN' => 'China',
    'CX' => 'Christmas Island',
    'CC' => 'Cocos (Keeling) Islands',
    'CO' => 'Colombia',
    'KM' => 'Comoros',
    'CG' => 'Congo',
    'CD' => 'Congo, DR,',
    'CK' => 'Cook Islands',
    'CR' => 'Costa Rica',
    'CI' => 'Cote d\'ivoire',
    'HR' => 'Croatia',
    'CU' => 'Cuba',
    'CY' => 'Cyprus',
    'CZ' => 'Czech Republic',
    'DK' => 'Denmark',
    'DJ' => 'Djibouti',
    'DM' => 'Dominica',
    'DO' => 'Dominican Republic',
    'EC' => 'Ecuador',
    'EG' => 'Egypt',
    'SV' => 'El Salvador',
    'GQ' => 'Equatorial Guinea',
    'ER' => 'Eritrea',
    'EE' => 'Estonia',
    'ET' => 'Ethiopia',
    'FK' => 'Falkland Islands',
    'FO' => 'Faroe Islands',
    'FJ' => 'Fiji',
    'FI' => 'Finland',
    'FR' => 'France',
    'GF' => 'French Guiana',
    'PF' => 'French Polynesia',
    'GA' => 'Gabon',
    'GM' => 'Gambia',
    'GE' => 'Georgia',
    'DE' => 'Germany',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GR' => 'Greece',
    'GL' => 'Greenland',
    'GD' => 'Grenada',
    'GP' => 'Guadeloupe',
    'GU' => 'Guam',
    'GT' => 'Guatemala',
    'GG' => 'Guernsey',
    'GN' => 'Guinea',
    'GW' => 'Guinea-Bissau',
    'GY' => 'Guyana',
    'HT' => 'Haiti',
    'HN' => 'Honduras',
    'HK' => 'Hong Kong',
    'HU' => 'Hungary',
    'IS' => 'Iceland',
    'IN' => 'India',
    'ID' => 'Indonesia',
    'IR' => 'Iran',
    'IQ' => 'Iraq',
    'IE' => 'Ireland',
    'IL' => 'Israel',
    'IT' => 'Italy',
    'JM' => 'Jamaica',
    'JP' => 'Japan',
    'JE' => 'Jersey',
    'JO' => 'Jordan',
    'KZ' => 'Kazakhstan',
    'KE' => 'Kenya',
    'KI' => 'Kiribati',
    'KP' => 'Korea, DPR',
    'KR' => 'Korea',
    'KW' => 'Kuwait',
    'KG' => 'Kyrgyzstan',
    'LA' => 'Lao, DPR',
    'LV' => 'Latvia',
    'LB' => 'Lebanon',
    'LS' => 'Lesotho',
    'LR' => 'Liberia',
    'LY' => 'Libyan Arab Jamahiriya',
    'LI' => 'Liechtenstein',
    'LT' => 'Lithuania',
    'LU' => 'Luxembourg',
    'MO' => 'Macao',
    'MK' => 'Macedonia',
    'MG' => 'Madagascar',
    'MW' => 'Malawi',
    'MY' => 'Malaysia',
    'MV' => 'Maldives',
    'ML' => 'Mali',
    'MT' => 'Malta',
    'MH' => 'Marshall Islands',
    'MQ' => 'Martinique',
    'MR' => 'Mauritania',
    'MU' => 'Mauritius',
    'YT' => 'Mayotte',
    'MX' => 'Mexico',
    'FM' => 'Micronesia',
    'MD' => 'Moldova',
    'MC' => 'Monaco',
    'MN' => 'Mongolia',
    'ME' => 'Montenegro',
    'MS' => 'Montserrat',
    'MA' => 'Morocco',
    'MZ' => 'Mozambique',
    'MM' => 'Myanmar',
    'NA' => 'Namibia',
    'NR' => 'Nauru',
    'NP' => 'Nepal',
    'NL' => 'Netherlands',
    'AN' => 'Netherlands Antilles',
    'NC' => 'New Caledonia',
    'NZ' => 'New Zealand',
    'NI' => 'Nicaragua',
    'NE' => 'Niger',
    'NG' => 'Nigeria',
    'NU' => 'Niue',
    'NF' => 'Norfolk Island',
    'MP' => 'Northern Mariana Islands',
    'NO' => 'Norway',
    'OM' => 'Oman',
    'PK' => 'Pakistan',
    'PW' => 'Palau',
    'PA' => 'Panama',
    'PG' => 'Papua New Guinea',
    'PY' => 'Paraguay',
    'PE' => 'Peru',
    'PH' => 'Philippines',
    'PN' => 'Pitcairn',
    'PL' => 'Poland',
    'PT' => 'Portugal',
    'PR' => 'Puerto Rico',
    'QA' => 'Qatar',
    'RE' => 'Reunion',
    'RO' => 'Romania',
    'RU' => 'Russian Federation',
    'RW' => 'Rwanda',
    'SH' => 'Saint Helena',
    'KN' => 'Saint Kitts and Nevis',
    'LC' => 'Saint Lucia',
    'PM' => 'Saint Pierre and Miquelon',
    'VC' => 'Saint Vincent',
    'WS' => 'Samoa',
    'SM' => 'San Marino',
    'ST' => 'Sao Tome and Principe',
    'SA' => 'Saudi Arabia',
    'SN' => 'Senegal',
    'RS' => 'Serbia',
    'SC' => 'Seychelles',
    'SL' => 'Sierra Leone',
    'SG' => 'Singapore',
    'SK' => 'Slovakia',
    'SI' => 'Slovenia',
    'SB' => 'Solomon Islands',
    'SO' => 'Somalia',
    'ZA' => 'South Africa',
    'ES' => 'Spain',
    'LK' => 'Sri Lanka',
    'SD' => 'Sudan',
    'SR' => 'Suriname',
    'SZ' => 'Swaziland',
    'SE' => 'Sweden',
    'CH' => 'Switzerland',
    'SY' => 'Syrian Arab Republic',
    'TW' => 'Taiwan',
    'TJ' => 'Tajikistan',
    'TZ' => 'Tanzania',
    'TH' => 'Thailand',
    'TL' => 'Timor-Leste',
    'TG' => 'Togo',
    'TK' => 'Tokelau',
    'TO' => 'Tonga',
    'TT' => 'Trinidad and Tobago',
    'TN' => 'Tunisia',
    'TR' => 'Turkey',
    'TM' => 'Turkmenistan',
    'TC' => 'Turks and Caicos Islands',
    'TV' => 'Tuvalu',
    'UG' => 'Uganda',
    'UA' => 'Ukraine',
    'AE' => 'United Arab Emirates',
    'GB' => 'United Kingdom',
    'US' => 'United States',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistan',
    'VU' => 'Vanuatu',
    'VA' => 'Vatican City State',
    'VE' => 'Venezuela',
    'VN' => 'Viet Nam',
    'VG' => 'Virgin Islands, British',
    'VI' => 'Virgin Islands, U.S.',
    'WF' => 'Wallis And Futuna',
    'YE' => 'Yemen',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabwe',
);

/*
    public $name;
    public $address;
    public $city;
    public $zip;
    public $state;
    public $country;
    public $cc_number;
    public $cc_cvn; 
*/
$masked_cc_num = '';
if($user->is_logged && $user->is_cc_data_saved){
   $masked_cc_num = 'Your saved CC will be charged, you can type another CC number here';
   if( preg_match('!\d+(?P<last>\d{4})$!', $user->cardholder_data->cc_number, $match )){
       $masked_cc_num =  str_repeat('X', strlen($user->cardholder_data->cc_number)-4).$match['last'];
   }  
}
 

$fields = array(
    'name' => array(
        'label' => 'Cardholder Name',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => $user->cardholder_data->name,            
        ),
        'error' => isset($user->cardholder_data->errors['name']) ? true : false
    ),
    'address' => array(
        'label' => 'Address',
        'attributes' => array (
            'value' => $user->cardholder_data->address        
        )
    ),
    'city' => array(
        'label' => 'City',
        'attributes' => array (
            'value' => $user->cardholder_data->city, 
        )
    ),
    'zip' => array(
        'label' => 'Zip/Postal',
        'attributes' => array (
            'value' => $user->cardholder_data->zip,                        
        )
    ),
    'state' => array(
        'label' => 'State',
        'attributes' => array (
            'value' => $user->cardholder_data->state,        
        ),
        'dropdown' => true,
        'values'   => $states
    ),
    'country' => array(
        'label' => 'Country',
        'attributes' => array (
            'value' => $user->cardholder_data->country,        
        ),
        'dropdown' => true,
        'values'   => $countries        
    ),
    'cc_number' => array(
        'label' => 'CC Number',
        'attributes' => array (
            'data-mandatory' => $user->is_logged && $user->is_cc_data_saved ? 'false' : 'true',
            'value'          => '',
            'placeholder'    => $user->is_logged && $user->is_cc_data_saved ? $masked_cc_num : ''
        ),
        'error' => isset($user->cardholder_data->errors['cc_number']) ? true : false
    ),    
    'cc_cvn' => array(
        'label' => 'CC CVN',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => '',
            'placeholder' => $user->is_logged && $user->is_cc_data_saved ? 'For your security, CVN was not saved, please type it here' : ''                        
        ),
        'error' => isset($user->cardholder_data->errors['cc_cvn']) ? true : false
    ),
    /*
    'amount' => array(
        'label' => 'Amount to be charged from your CC to you Papercut account',
        'attributes' => array(
            'data-mandatory' => 'true',
            'value' => $user->transaction_data->amount,
            'id'    => 'amount'
        ),
        'error' => isset($user->transaction_data->errors['amount']) ? true : false
    )
    */                                                             
); 

$payment_values = array();
foreach($user->recurring_payment_data->supported_values as $value){
    $payment_values[$value]='$'.$value;
}




?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MicroOffice Payment Portal</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php $this->load->view(
            'common/partials/navbar', 
            array(
                'user'         => $user,
                'papercut_url' => $papercut_url
            )
        ); ?>
        
        <div class="container">
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8"> 
                <?php 
                if( isset($status) && $status ){
                    if( $status == MY_Controller::SUCCESS ){
                        $class  = 'alert alert-success';
                        $status = 'Your credit card has been charged, and amount <b>USD '.$user->transaction_data->amount.'</b> was added to your Papercut account';
                    } else {
                        $class = 'alert alert-danger';
                    }
                    echo '<div class="'.$class.'">'.$status.'</div>';
                }
                ?>
                </div>
                <div class="col-md-2"> </div>    
            </div>
            <div class="row">
                <div class="col-md-2"> </div>
                
                <div class="col-md-8">
                    <!-- CC Form -->
                    <form role="form" action="<?php echo site_url('charge/recharge/'.$user->username.'/'.$user->papercut_id.'/')?>" method="post">

                        <?php echo $bootstrap_form_helper->generate_form_elements( $fields ) ?>

                        <div class="form-group">
                            <label>Amount to charge</label>
                            <select id="amount" name="amount"class="form-control">
                                <option value="10">$10</option>
                                <option value="25">$25</option>
                                <option value="50">$50</option>
                            </select>
                        </div>                      

                        <div class="form-inline">
                            <div class="form-group">
                                <label >CC Expire Month:</label>
                            </div>                        
                            <div class="form-group">
                                <?php echo form_dropdown('expire_month', $user->cardholder_data->get_months(), $user->cardholder_data->get_month(), 'class="form-control" data-mandatory="true" ' )?>
                            </div>
                            <div class="form-group">
                                <label >CC Expire Year:</label>
                            </div>                             
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword2">Charge this CC with:</label>
                                <?php echo form_dropdown('expire_year',  $user->cardholder_data->get_years(), $user->cardholder_data->get_year(),  'class="form-control"  data-mandatory="true" ' )?>
                            </div>
                        </div>
                                                
                        <?php if( $user->is_logged ){ ?>                                              
                        <div class="checkbox">
                            <label>
                              <input type="checkbox" name="save_cc" id="save_cc" data-toggle="modal" data-target="#cc_modal">Save credit card information
                            </label>
                        </div>
                        <?php } ?>

                        <div class="form-inline">
                            <div class="form-group">
                                <label >When account balance drops below:</label>
                            </div>                        
                            <div class="form-group">
                                <?php echo form_dropdown('balance_limit', $payment_values,$user->recurring_payment_data->balance_limit, 'class="form-control"' )?>
                            </div>
                            <div class="form-group">
                                <label >Charge this CC with</label>
                            </div>                             
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword2">Charge this CC with:</label>
                                <?php echo form_dropdown('balance_recharge',$payment_values,$user->recurring_payment_data->balance_recharge, 'class="form-control"' )?>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox" id="recurring_payment" <?php echo $user->has_recurring_payment ? 'checked="checked"': '' ?> data-toggle="modal" name="recurring_payment" data-target="#cc_modal_recurring">Activate recuring payment
                                </label>
                            </div>
                        </div>                     

                        <button type="submit" id="recharge"     <?php isset($status) && $status ? ' disbled="disabled" ' :'' ?>  class="btn btn-primary">Recharge</button>
                        <input  type="hidden" name="token"       value="<?php echo $token ?>">
                        <input  type="hidden" name="username"    value="<?php echo $user->username ?>">
                        <input  type="hidden" name="papercut_id" value="<?php echo $user->papercut_id ?>">
                        <input  type="hidden" name="user_token"  value="<?php echo $user_token ?>">


                    </form>
                    
                     
                    <!-- CC Form End -->
                </div>
               
                
                <div class="col-md-2"></div>
            </div>    
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="cc_modal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Terms of service</h4>
              </div>
              <div class="modal-body">
                <p>Terms of saving CC data</p>
              </div>
              <div class="modal-footer">
                <button type="button" id="modal_decline" class="btn btn-default" data-dismiss="modal">Decline</button>
                <button type="button" id="modal_accept"class="btn btn-primary" data-dismiss="modal">Accept</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- Modal End -->
        
        <!-- Modal -->
        <div class="modal fade" id="cc_modal_recurring">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Terms of service</h4>
              </div>
              <div class="modal-body">
                <p>Terms of saving CC data and Recurring payments</p>
              </div>
              <div class="modal-footer">
                <button type="button" id="modal_decline_recurring" class="btn btn-default" data-dismiss="modal">Decline</button>
                <button type="button" id="modal_accept"class="btn btn-primary" data-dismiss="modal">Accept</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- Modal End -->        
                
        <script type="text/javascript">
        $(document).ready(
            function(){
                //Validation 
                $('#recharge').click(                
                    
                    function(){
                        $('.has-error').removeClass('has-error');
                        var valid = true;
                        $('[data-mandatory="true"]').each(
                            function(){
                                if( $(this).val() == '' ){
                                    $(this).parent().addClass('has-error');
                                    valid = false;
                                }
                            }
                        )
                        
                        if( valid == true )   {
                            valid = confirm('This will charge your credit card with USD '+$('#amount').val()+' and recharge your PaperCut account, do you want to proceed?')
                        }
                        return valid;
                    }
                )

                $('#modal_decline').click(
                    function(){
                        $('#save_cc').removeAttr('checked');
                    }
                )
                
                $('#modal_decline_recurring').click(
                    function(){
                        $('#recurring_payment').removeAttr('checked');
                    }
                )
            }
        );
        </script>        
    </body>    
</html>
