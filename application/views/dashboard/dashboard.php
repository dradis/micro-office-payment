<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MicroOffice Payment Portal</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php $this->load->view(
            'common/partials/navbar', 
            array(
                'user'         => $user,
                'papercut_url' => $papercut_url,
            )
        ); ?>
        
        <div class="container">
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8">                
                    <?php 
                    if( isset($status) && $status ){
                        if( $status == Account::SUCCESS ){
                            $class  = 'alert alert-success';
                            $status = 'Your credit card has been charged, and amount <b>USD '.$user->transaction_data->amount.'</b> was added to your Papercut account';
                        } else {
                            $class = 'alert alert-danger';
                        }
                        echo '<div class="'.$class.'">'.$status.'</div>';
                    }
                    ?>
                </div>
                <div class="col-md-2"> </div>    
            </div>
            
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8">
                    <?php 
                    ?>
                </div>
                <div class="col-md-2"> </div>    
            </div> 
                       
            <div class="row">
                <div class="col-md-2"> </div>
                
                <div class="col-md-8">
                    <a class="btn btn-primary btn-lg btn-block" href="<?php echo site_url('charge/user_papercut/'.$user->username.'/'.$user->papercut_id.'/')?>">Charge Account</a>
                    <?php if(!$user->is_logged ) {?>
                        <!-- If not logged, display form -->
                        <?php if($user->is_registered) {?>
                            <!-- Existing user -->
                            <a href="<?php echo site_url('login')?>" class="btn btn-info btn-lg btn-block">Login</a>

                        <?php }?>
                    <?php } ?>
                </div>
               
                
                <div class="col-md-2"></div>
            </div>    
        </div>
               
        <script type="text/javascript">
        $(document).ready(
            function(){
            }
        );
        </script>        
    </body>    
</html>