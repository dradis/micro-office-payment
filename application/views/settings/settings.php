<?php

$fields = array(
    'old_password' => array(
        'label' => 'Old Password',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => '',
            'type'  => 'password'            
        ),
        'error' => isset($user->errors['password']) ? true : false
    ),
    'email' => array(
        'label' => 'Email',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => $user->email,
            'type'  => 'email'
        ),
        'error' => isset($user->errors['email']) ? true : false
    ),    
    'password' => array(
        'label' => 'Password',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => '',
            'id'    => 'password',
            'type'  => 'password'
        ),
        'error' => isset($user->errors['password']) ? true : false
    ),
    'password2' => array(
        'label' => 'Retype Password',
        'attributes' => array (
            'data-mandatory' => 'true',
            'value' => '',
            'id'    => 'password2',
            'type'  => 'password'
        ),
        'name_override' => 'password2',
        'error' => isset($user->errors['password']) ? true : false
    ),        
);

$masked_cc_num = '';
if($user->is_logged && $user->is_cc_data_saved){
   $masked_cc_num = 'Your saved CC will be charged, you can type another CC number here';
   if( preg_match('!\d+(?P<last>\d{4})$!', $user->cardholder_data->cc_number, $match )){
       $masked_cc_num =  str_repeat('X', strlen($user->cardholder_data->cc_number)-4).$match['last'];
   }  
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MicroOffice Payment Portal</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php $this->load->view(
            'common/partials/navbar', 
            array(
                'user'    => $user,
                'papercut_url' => $papercut_url
            )
        ); ?>
        
        <div class="container">
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8"> 
                <?php 
                if( isset($status) && $status ){
                    if( $status == MY_Controller::SUCCESS ){
                        $class  = 'alert alert-success';
                        $status = 'Your account settings have been successfully changed';
                    } else {
                        $class = 'alert alert-danger';
                    }
                    echo '<div class="'.$class.'">'.$status.'</div>';
                }
                ?>
                </div>
                <div class="col-md-2"> </div>    
            </div>
            <div class="row">
                <div class="col-md-2"> </div>
                
                <div class="col-md-8">
                    <fieldset>
                        <!-- CC Form -->
                        <form role="form" action="<?php echo site_url('settings/change_settings')?>" method="post">
                          <?php echo $bootstrap_form_helper->generate_form_elements($fields) ?>
                                                                   
                          <button type="submit" id="recharge" class="btn btn-primary">Save Changes</button>
                          <button type="submit" id="recharge" class="btn btn-default">Discard</button>
                          <input  type="hidden" name="token" value="<?php echo $token ?>">
                        </form>
                    </fieldset>
                    <!-- CC Form End -->
                </div>
               
                
                <div class="col-md-2"></div>
            </div> 
            <div class="row"></div>
            <div class="row"></div>
            <div class="row"></div>
            <div class="row">
                <div class="col-md-2"> </div>
                
                <div class="col-md-8">
                    <?php if( $user->is_cc_data_saved ){ ?>
                        <p>
                            <h4>Your saved card number is: <?php echo $masked_cc_num?></h4>
                            <fieldset>
                                <!-- CC Form -->
                                <form role="form" action="<?php echo site_url('settings/delete_cc_data/'.$user->username.'/'.$user->papercut_id.'/')?>" method="post">          
                                  <button type="submit" class="delete btn btn-info btn-lg btn-block">Delete stored CC Data</button>
                                </form>
                            </fieldset>
                        </p>
                    <?php } ?>
                    <!-- CC Form End -->
                </div>
               
                
                <div class="col-md-2"></div>
            </div>
            
            <div class="row">
                <div class="col-md-2"> </div>
                
                <div class="col-md-8">
                    <?php if( $user->has_recurring_payment ){ ?>
                        <p>
                            <h4>Your saved card will be charged with USD  <b><?php echo $user->recurring_payment_data->balance_limit ?></b> when Papercut balance drops below USD  <b><?php echo $user->recurring_payment_data->balance_recharge ?></b></h4>
                            <fieldset>
                                <!-- CC Form -->
                                <form role="form" action="<?php echo site_url('settings/delete_recurring_payment_data/'.$user->username.'/'.$user->papercut_id.'/')?>" method="post">
                                  <div class="form-group">          
                                    <button type="submit" class="delete btn btn-info btn-lg btn-block">Delete recurring payment data</button>
                                  </div>
                                </form>
                            </fieldset>
                        </p>
                    <?php } ?>
                    <!-- CC Form End -->
                </div>
               
                
                <div class="col-md-2"></div>
            </div>                           
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="cc_modal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Terms of service</h4>
              </div>
              <div class="modal-body">
                <p>Terms of saving CC data</p>
              </div>
              <div class="modal-footer">
                <button type="button" id="modal_decline" class="btn btn-default" data-dismiss="modal">Decline</button>
                <button type="button" id="modal_accept"class="btn btn-primary" data-dismiss="modal">Accept</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- Modal End -->
                
        <script type="text/javascript">
        $(document).ready(
            function(){
                //Validation 
                $('#recharge').click(                
                    
                    function(){
                        $('.has-error').removeClass('has-error');
                        var valid = true;
                        $('[data-mandatory="true"]').each(
                            function(){
                                if( $(this).val() == '' ){
                                    $(this).parent().addClass('has-error');
                                    valid = false;
                                }
                            }
                        )
                        
                        return valid;
                    }
                )

                $('#modal_decline').click(
                    function(){
                        $('#save_cc').removeAttr('checked');
                    }
                )
                
                $('.delete').click(
                    function(){
                        if(confirm('This will permanently delete your stored CC data, are you sure that you want to continue?')){
                            return true;
                        } else {
                            return false;
                        }
                    }
                )
            }
        );
        </script>        
    </body>    
</html>
