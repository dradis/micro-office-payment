<?php
 

?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MicroOffice Payment Portal</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </head>

    <body>
        <?php $this->load->view(
            'common/partials/navbar', 
            array(
                'user'         => $user,
                'papercut_url' => $papercut_url,
            )
        ); ?>
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Cardholder</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Zip</th>
                                <th>State</th>
                                <th>Country</th>
                                <th>Amount</th>
                                <th>Timestamp</th>
                                <th>Status</th>
                                <th>Approval code</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach( $transactions as $t ){
                                echo '<tr class="'.($t->response_error ? 'danger' : 'success').'">'.
                                        '<td>'.$t->billing_name.'</td>'.
                                        '<td>'.$t->billing_address_1.'</td>'.
                                        '<td>'.$t->billing_city.'</td>'.
                                        '<td>'.$t->billing_zip_code.'</td>'.
                                        '<td>'.$t->billing_state.'</td>'.
                                        '<td>'.$t->billing_country.'</td>'.
                                        '<td>'.$t->total_charge.'</td>'.
                                        '<td>'.$t->queued_dt.'</td>'.
                                        '<td>'.($t->response_error ? 'Failed' : 'Successfull').'</td>'.
                                        '<td>'.($t->response_approved ? $t->response_approved : $t->response_error).'</td>'.
                                        '<td>'.($t->notes).'</td>'.
                                     '</tr>';    
                            }
                             
                        ?>
                        </tbody>
                    </table>
                </div>  
            </div>    
        </div>
 
                
        <script type="text/javascript">
        $(document).ready(
            function(){
                //Validation 
                $('#recharge').click(                
                    
                    function(){
                        $('.has-error').removeClass('has-error');
                        var valid = true;
                        $('[data-mandatory="true"]').each(
                            function(){
                                if( $(this).val() == '' ){
                                    $(this).parent().addClass('has-error');
                                    valid = false;
                                }
                            }
                        )
                        
                        if( valid == true )   {
                            valid = confirm('This will charge your credit card with USD'+$('#amount').val()+' and recharge your PaperCut account, do you want to proceed?')
                        }
                        return valid;
                    }
                )

                $('#modal_decline').click(
                    function(){
                        $('#save_cc').removeAttr('checked');
                    }
                )
            }
        );
        </script>        
    </body>    
</html>
