<?php
class Login extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    public function index(){
        try{
            $this->_load_login();
        } catch( Exception $e ){
            $this->_redirect_to_papercut();
        }
    }
    
    protected function _load_login($status = null){
        $this->session->sess_destroy();
        $this->load->view(
            'login/login',
            array(
                'papercut_url' => $this->config->item('papercut_backoffice'),
                'status' => $status
            )
        );
    }    
    
    public function create_account(){
        try{
            $this->_load_login();
            $this->_load_create_form();
        } catch( Exception $e ){
            $this->_redirect_to_papercut();
        }
    }
    
    protected function _load_create_form(user $user = null, $status = null){
        $user = $user ? $user : $this->marshaller->marshall_papercut_user();
        $this->load->view(
            'login/create_account',
            array(
                'papercut_url' => $this->config->item('papercut_backoffice'),
                'user'         => $user,
                'token'        => $this->_generate_user_token($user),
                'status'       => $status,
                'bootstrap_form_helper' => $this->bootstrap_form_helper
            )
        );        
    }
       
    public function new_account(){
        try{
            $this->_new_account();
            
        } catch( InvalidTokenException $e){
            $this->_redirect_to_papercut();    
        } catch( PasswordsDontMatchException $e ){
            $this->_load_create_form( 
                $e->user,
                $e->getMessage()
            );
        } catch( InvalidPersonsTblRecordException $e ){
            $this->_load_create_form( 
                $e->user,
                $e->getMessage()
            );            
        }
    }
    
    protected function _new_account(){
        $user = new user();
        $user->username    = $this->input->post('username');
        $user->papercut_id = $this->input->post('papercut_id');
        $user->first_name  = $this->input->post('first_name');
        $user->last_name   = $this->input->post('last_name');        
        $user->email       = $this->input->post('email');        
        $token = $this->input->post('token');
    
        $this->_validate_user_token( $token, $user );
        
        $password = $this->input->post('password');
        if( $password != $this->input->post('password2')){
            $user->set_error('password',true);
            throw new PasswordsDontMatchException( $user, 'Invalid passwords');
        }
 
        $user->password = $password;

        $user->verify_persons_tbl_record_data();
        
        if( $this->account_model->is_user_unique( $user )){
            $this->account_model->create_new_account($user);    
        } else {
            throw new DuplicateUserException( $user, 'User already exists');
        }
        
        $this->_load_login();     
    }
    
    public function authenticate(){
        try{
            $this->_authenticate_user();
        } catch( Exception $e ){
            $this->_load_login( $e->getMessage());
        }        
    }
        
    protected function _authenticate_user(){
        $user = new user();
        if( !($username = $this->input->post('username')) || !($pass = $this->input->post('password')) ){
            throw new InvalidCredentialsException();
        } else {
            $user->username = $username;
            $user->password = $pass;
        }
        
        $user = $this->account_model->authenticate( $user );
        $this->_set_user($user);
        header('Location: '. site_url('charge/user_papercut/'.$user->username.'/'.$user->papercut_id));
    }
}
  
class PasswordsDontMatchException extends PaymentPortalException{}
class DuplicateUserException      extends PaymentPortalException{}



