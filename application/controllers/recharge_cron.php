<?php
class Recharge_cron extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('recharge_model');
    }
    
    public function check_and_recharge(){
         
        $recharge_users = $this->recharge_model->get_payment_portal_users_with_recurring_payments();
        
        foreach( $recharge_users as &$user ){
            try{
                $this->_check_and_recharge_user($user);
            } catch(PaperCutException $e ){
                
            } catch(FirstDataTransactionFailed $e ){
                
            }
        }

    }
    
    protected function _check_and_recharge_user($user){
        $user->balance = $this->api_papercut->get_user_balance($user);
        if( $this->_is_due_for_recharge($user)){
            $user->transaction_data->amount = $user->recurring_payment_data->balance_recharge;
            $user->transaction_data->description = $this->_get_description($user);
             
            $user = $this->api_firstdata->charge_credit_card( $user );
            $this->api_papercut->adjuset_user_account($user);
            $this->_send_email_receipt($user);
            $this->account_model->log_transactions($user);
        }        
    }
    
    protected function _is_due_for_recharge($user){
        $recharge = false;
        if( $user->balance < $user->recurring_payment_data->balance_limit ){
            $recharge = true;
        }
        return $recharge;
    }
    
    protected function _get_description($user){
        $limit  = $user->recurring_payment_data->balance_limit;
        $amount = $user->recurring_payment_data->balance_recharge; 
        $desc = "Your account balance dropped below USD$limit, your account was automatically recharged with $amount from your credit card";
        $desc.= "If you wish to disable this feature, you can deactivate it from your MicroOffice Payment Portal account";
        return $desc;
    }

    public function start_cron_job(){
        set_time_limit(20000);
        echo 'Cron Job started (20 tasks will be done)<br>';
        $cron_task_limit = 20;
        for( $i=0; $i<$cron_task_limit; $i++){
            echo 'Executing cron task<br>';
            $this->_do_cron();
            sleep(300);   
        }
        echo 'Cron Job stopped<br>';
    }
    
    protected function _do_cron(){
        $curl = curl_init();
 
        curl_setopt($curl, CURLOPT_VERBOSE, TRUE);  
        curl_setopt($curl, CURLOPT_URL, site_url('recharge_cron/check_and_recharge') );
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec ($curl);
        
        curl_close ($curl);        
    }
}


