<?php
class Transactions extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->_authenticate();
    }
        
    public function user(){ 
        try{
            $this->_display_user_transactions();    
        } catch( InvalidUserException $e){
            $this->_redirect_to_login_page( $e->user);
        }
    }
    
    public function _display_user_transactions(){
        $user = $this->_initialize_user();
        $transactions = $this->account_model->load_transactios( $user );
        $this->load->view(
            'transactions/transactions',
            array(
                'user'         => $user,
                'transactions' => $transactions,
                'papercut_url' => $this->config->item('papercut_backoffice')
            )
        );
    }
}
