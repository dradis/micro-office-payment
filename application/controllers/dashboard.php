<?php
class Dashboard extends MY_Controller{
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->user();
    }
    public function user(){
        try{
            $this->_load_dashboard();
        } catch( AccountMarshallException $e){
            $this->_redirect_to_papercut();    
        } catch( PaperCutException $e){
            $this->_redirect_to_papercut();    
        }
    }
    
    protected function _load_dashboard(){
        $user = $this->marshaller->marshall_papercut_user();
        $this->api_papercut->check_if_user_exists($user);
        $user->balance = $this->api_papercut->get_user_balance($user);
        $user->is_registered = $this->account_model->user_exists_in_db($user);
        $user->is_logged     = $this->is_user_logged($user);
        $this->load->view(
            'dashboard/dashboard', 
            array( 
                'user'    => $user,
                'token'   => $this->_generate_token(),
                'status'  => null,
                'papercut_url' => $this->config->item('papercut_backoffice'),
            )
        );    
    }
}
