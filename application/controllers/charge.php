<?php
class Charge extends MY_Controller{
    
    protected $user_has_cc_saved = false;
     
    public function __construct(){
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->database();
    }
    
    public function user_papercut(){
        try{
            $this->_display_charge_form();
        } catch( AccountMarshallException $e){
            $this->_redirect_to_login_page();    
        } catch( PapercutInvalidUserException $e){
            $this->_redirect_to_login_page();    
        } 
    }
    
    protected function _display_charge_form(){
        $user = $this->marshaller->marshall_papercut_user();
        $this->api_papercut->check_if_user_exists($user);
        
        if( $this->account_model->user_has_saved_cc_details( $user )){
            if( !$this->is_user_logged( $user )){
                $this->_redirect_to_login_page( $user );    
            } else {
                $user = $this->_load_cc_data($user);
                $this->_redirect_to_payment_page( $user );
            }
        } else {
            $this->_redirect_to_payment_page( $user );
        }     
    }
    
    public function recharge(){
        try{            
            $this->_charge_cc_and_update_papercut();
        } catch( PapercutInvalidUserException $e ){
            header('Location: '.$this->config->item('papercut_backoffice'));
        } catch( FirstDataTransactionFailed $e ){
            
            $this->account_model->log_transactions($e->user);
            
            $this->_redirect_to_payment_page(
                $e->user,
                $e->getMessage()
            );
        } catch( PaymentPortalException $e ){
            $this->_redirect_to_payment_page(
                $e->user,
                $e->getMessage()
            );            
        }
    }
    
    protected function _charge_cc_and_update_papercut(){
        $this->_validate_token( $this->input->post('token') );
        
        $user = new user();
        $user->username = $this->input->post('username');
        $user->papercut_id = $this->input->post('papercut_id');
        
        $this->_validate_user_token( 
            $this->input->post('user_token'),
            $user
        );
 
        $db_user_data = new user();
        if( $this->is_user_logged( $user ) ){
            
            $user = $this->_initialize_user();
            $db_user_data = $this->_load_cc_data(clone($user));
            //Combine db & post data
            $user = $this->marshaller->marshall_cardholder_data( $user, $db_user_data );
            
            //Save CC Form if user opts.             
            if( $this->input->post('save_cc') || $this->input->post('recurring_payment') ){
                $this->account_model->save_cc_data($user);
            }
            
            $user = $this->account_model->get_recurring_payment_data($user);
            //Save/delete recurring payment data
            if( $this->input->post('recurring_payment')){
                $user = $this->marshaller->marshall_recurring_payment_data( $user);
                $this->account_model->save_recurring_payment( $user );
            } else if( $user->recurring_payment_data->verify() ){
                $this->account_model->delete_recurring_payment( $user );
            }
                        
        } else {
            $user = $this->marshaller->marshall_cardholder_data( $user, $db_user_data );
        }
        
        //Updates User object with successfull transaction data
        $user = $this->api_firstdata->charge_credit_card( $user );
        //Adjust account
        $this->api_papercut->adjuset_user_account( $user );
        //Email/LOG description
        $user->transaction_data->description = $this->_get_description($user);
        //Save transaction data to DB
        $this->account_model->log_transactions( $user );
        //Send confirmation email
        @$this->_send_email_receipt( $user );
        //Go to Payment Page, and display result
        $this->_redirect_to_payment_page(
            $user,
            self::SUCCESS
        );     
    }
    
    protected function _redirect_to_payment_page( $user = null, $status = '' ){    
        $user->is_logged = $this->is_user_logged( $user);
        $user->balance   = $this->api_papercut->get_user_balance($user);

        $this->load->view(
            'account/payment', 
            array( 
                'user'    => $user,
                'token'   => $this->_generate_token(),
                'status'  => $status,
                'papercut_url' => $this->config->item('papercut_backoffice'),
                'bootstrap_form_helper'  => $this->bootstrap_form_helper,
                'user_token'  => $this->_generate_user_token($user),
            )
        );    
    }


    
    protected function _get_description(user $user){
        $amount = $user->transaction_data->amount;
        $desc   = 'Your credit card has been charged with USD'.$amount.', and the same amount was added to your Papercut Account';
        return $desc;
    }
}


