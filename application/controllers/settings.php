<?php
class Settings extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->_authenticate();
    }
    
    public function user(){
        $this->_redirect_to_settings($this->_initialize_user());
    }
    
    public function change_settings(){
        try{
            $this->_change_settings();
        } catch( InvalidUserException $e){
            $this->_redirect_to_settings(
                $e->user,
                'Incorrect old password'
            );
        } catch (PaymentPortalException $e ) {
            $this->_redirect_to_settings(
                $e->user,
                'Your passwords dont match'
            );            
        }
    }
    
    public function _change_settings(){
        $user = $this->_initialize_user();
        if( $this->input->post('old_password') !== $user->password ){
            throw new InvalidUserException($user); 
        } 
        
        $user->password = $this->input->post('password');
        if( $user->password != $this->input->post('password2')){
            throw new PaymentPortalException($user);
        }
        $user->email = $this->input->post('email');
        
        $this->account_model->save_new_account_settings($user);
        $this->_redirect_to_settings($user, self::SUCCESS);
    }
    
    public function delete_cc_data(){
        try{
            $this->_delete_cc_data();
        } catch( PaymentPortalException $e ){
            $this->_redirect_to_login_page();
        }
    }
    
    protected function _delete_cc_data(){
        $user = $this->_initialize_user();
        $this->account_model->delete_cc_data($user);
        $this->account_model->delete_recurring_payment($user);
        $user->has_recurring_payment = false;
        $user->is_cc_data_saved = false;
        $this->_redirect_to_settings($user, self::SUCCESS);
    }
    
    public function delete_recurring_payment_data(){
        try{
            $this->_delete_recurring_payment_data();
        } catch( PaymentPortalException $e ){
            $this->_redirect_to_login_page();
        }        
    }
    
    protected function _delete_recurring_payment_data(){
        $user = $this->_initialize_user();
        $this->account_model->delete_recurring_payment($user);
        $user->has_recurring_payment = false;
        $this->_redirect_to_settings($user, self::SUCCESS);
    }
    
    protected function _redirect_to_settings( $user, $status = null ){
        $user = $this->_load_cc_data($user);
        $this->load->view('settings/settings',
            array(
                'user'    => $user,
                'papercut_url' => $this->config->item('papercut_backoffice'),
                'token'    => $this->_generate_token(),
                'status'   => $status,
                'bootstrap_form_helper' => $this->bootstrap_form_helper
            )
        );        
    }
}
