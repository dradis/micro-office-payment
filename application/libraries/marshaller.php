<?php
class Marshaller{
    public $uri;
    public function marshall_papercut_user(){
        $user = new User();
        $user->username    = $this->uri->segment(3);        
        $user->papercut_id = $this->uri->segment(4);
        
        $ret_val = null;
        if( $user->is_valid()){
            $ret_val = $user;
        } else {
            throw new AccountMarshallException('User doesnt exist');
        }
        return $ret_val;
    }
    
    public function marshall_cardholder_data(user $user, user $db_data = null ){
    
        foreach( $user->cardholder_data as $field=>$value){
            if( $post_value = $this->input->post($field)){
                $user->cardholder_data->$field = $post_value;
            } else if( $db_data && isset($db_data->cardholder_data->$field) && $db_data->cardholder_data->$field){
                $user->cardholder_data->$field = $db_data->cardholder_data->$field;    
            }
        }
       
        if( $this->input->post('expire_month') && $this->input->post('expire_year')){
            $user->cardholder_data->cc_expire = $this->input->post('expire_month').$this->input->post('expire_year');
        }
        
        foreach( $user->transaction_data as $field=>$value){
            if( $post_value = $this->input->post($field)){
                $user->transaction_data->$field = $post_value;
            }            
        }
        
        $user->verify_payment_gateway_data();
        return $user;
    }
    
    public function marshall_persons_tbl_user(user $user){
        
    }
    
    public function initialize($uri, $input){
        $this->uri   = $uri;
        $this->input = $input;
    }
    
    public function marshall_recurring_payment_data(user $user){
        $user->recurring_payment_data->balance_limit    = $this->input->post('balance_limit');
        $user->recurring_payment_data->balance_recharge = $this->input->post('balance_recharge');
        $user->verify_recurring_payment_data();
        return $user;
    }    
}

class AccountMarshallException extends PaymentPortalException{}
