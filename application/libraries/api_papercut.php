<?php
class Api_papercut{
    
    protected $initialized = false;

    public function initialize( $server, $token){
        $this->token  = $token;
        $this->server = $server;
        $this->initialized = true;
    }
    
    public function check_if_user_exists(user $user){
        if( !$this->initialized ){
            throw new PaperCutException('Papercut API not initialized');
        }
        
        $request =
        '<methodCall>'.
           '<methodName>api.isUserExists</methodName>'.
           '<params>'.
               '<param>'.
                    '<value><string>'.$this->token.'</string></value>'.
               '</param>'.
               '<param>'.
                '<value><string>'.$user->username.'</string></value>'.
               '</param>'.
           '</params>'.
        '</methodCall>';
        
        $xml = $this->execute($request);
        return $this->parse_bool($xml);
    }
    
    public function adjuset_user_account(user $user ){
        if( !$user || !$user->transaction_data->amount ){
            throw new PaperCutException('Invalid params');
        }
        $comment = $user->transaction_data->description ? $user->transaction_data->description : $this->get_comment($user, $user->transaction_data->amount);
        
        $request =
        '<methodCall>'.
           '<methodName>api.adjustUserAccountBalance</methodName>'.
           '<params>'.
               '<param>'.
                    '<value><string>'.$this->token.'</string></value>'.
               '</param>'.
               '<param>'.
                '<value><string>'.$user->username.'</string></value>'.
               '</param>'.
               '<param>'.
                '<value><double>'.$user->transaction_data->amount.'</double></value>'.
               '</param>'.
               '<param>'.
                '<value><string>'.$comment.'</string></value>'.
               '</param>'.
           '</params>'.
        '</methodCall>';
        
        $xml = $this->execute($request);
        return $this->parse_bool($xml);
    }
    
    public function get_user_balance(user $user ){
        if( !$user ){
            throw new PaperCutException('Invalid params');
        }
 
        
        $request =
        '<methodCall>'.
           '<methodName>api.getUserAccountBalance</methodName>'.
           '<params>'.
               '<param>'.
                    '<value><string>'.$this->token.'</string></value>'.
               '</param>'.
               '<param>'.
                '<value><string>'.$user->username.'</string></value>'.
               '</param>'.
           '</params>'.
        '</methodCall>';
        
        $xml     = $this->execute($request);
        $balance = (string)$xml->params->param->value->double;
        return $balance;    
    }
    
    protected function execute( $payload ){

        $curl = curl_init();
        $headers = array( 
            'Content-Type: text/xml',
            'Content-Length: '.strlen($payload)
        );    
        
        curl_setopt($curl, CURLOPT_VERBOSE, TRUE);  
        curl_setopt($curl, CURLOPT_URL, $this->server );
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers );
        $result = curl_exec ($curl);

        $xml = null;
        if(curl_error($curl)){
            throw new PaperCutException('Error while executing request: '.curl_error($curl));
        } else {
            try{
                $xml = @new SimpleXMLElement($result);
                if( isset( $xml->fault )){
                    throw new PaperCutException('Fault response fomr Papercut');    
                }
            } catch( Exception $e){
                throw new PaperCutException('Invalid xml received from Papercut server');    
            }
        } 
        curl_close ($curl);
        
        return $xml;    
             
    }
    
    protected function parse_bool( SimpleXMLElement $xml ){
        $result = false;
        if( (string)$xml->params->param->value->boolean == '1'){
            $result = true;
        }
        return $result;   
    }
    
    
    protected function get_comment(user $user, $adjustment ){
        return "Papercut account adjustment for $user->username (USD$adjustment) ".gmdate('Y-m-d h:i:s');
    }
}

class PaperCutException extends PaymentPortalException{}
class PaperCutInvalidUserException extends PaymentPortalException{}