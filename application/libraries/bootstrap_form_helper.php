<?php
class bootstrap_form_helper{
    
    public function generate_form_elements($fields){
        $form = '';
        foreach( $fields as $field_name=>$field ){ 
          $form .=
                  '<div class="form-group  '.(isset($field['error']) && $field['error'] ? ' has-error has-feedback': '').'">'.
                        '<label >'.$field['label'].'</label>';
                        
                        if( isset($field['dropdown']) ){
                            $form .= form_dropdown( ( isset($field['name_override']) ? $field['name_override'] : $field_name) , $field['values'], $field['attributes']['value'], 'class="form-control  input-sm"');
                        } else {
                            $form .= '<input class="form-control  input-sm" name="'.( isset($field['name_override']) ? $field['name_override'] : $field_name).'" '.$this->get_attributes( $field ).'>';
                        }
                        
                        
                        if(isset($field['error']) && $field['error']){
                            '<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>';
                        }
          $form .= '</div>';
        }        
        return $form;
    }
    
    public function get_attributes($field){ 
        $array = array();
        foreach( $field['attributes'] as $attr=>$value){
            $array[] = $attr.'="'.$value.'"';
        }
        return implode(' ', $array);         
    }
}
