<?php
class Api_firstdata{
    
    const TRANSACTION_PURCHASE = '00';
    
    public function initialize( $wsdl, $hmac, $keyid, $gateway_code, $password ){
        $this->soap_client = new  SoapClientHMAC(
            $wsdl,
            $hmac,
            $keyid    
        );
        $this->gateway_code = $gateway_code;
        $this->password     = $password;  
    }
    
    public function charge_credit_card(user $user){
        $transaction_request = $this->create_transaction( $user );
        
        try{
            $transaction_response = $this->soap_client->SendAndCommit($transaction_request);
            //$user->transaction_data->xml_request  = $this->soap_client->transaction_request;
            //$user->transaction_data->xml_response = $this->soap_client->transaction_response;            
            
            $user->transaction_data->AVS               = $transaction_response->AVS;
            $user->transaction_data->Authorization_Num = $transaction_response->Authorization_Num;
            $user->transaction_data->CTR               = $transaction_response->CTR;
            if( !$transaction_response->Authorization_Num ){
                $user->transaction_data->error_message = (($error_message = (string)$transaction_response->Bank_Message) || ($error_message = (string)$transaction_response->EXact_Message)) ? 
                                 $error_message : 
                                 'Transaction failed, your credit card has not been charged';
                                 
                $user->transaction_data->error_code = (($error_code = (string)$transaction_response->EXact_Resp_Code) || ($error_code = (string)$transaction_response->EXact_Resp_Code)) ? $error_code : null;
                
                throw new FirstDataTransactionFailed($user, $error_message);
            }
        } catch (SoapFault $fault){
            throw new FirstDataTransactionFailed($user, 'Invalid response received from Payment Gateway, your credit card has not been charged');
        }
        
        return $user;
    }
    
    protected function create_transaction( user $user ){
        $request = array(
          "User_Name"          => "",
          "Secure_AuthResult"  => "",
          "Ecommerce_Flag"     => "",
          "XID"                => "",
          "ExactID"            => $this->gateway_code,                    //Payment Gateway
          "CAVV"               => $user->cardholder_data->cc_cvn,
          "Password"           => $this->password,                                //Gateway Password
          "CAVV_Algorithm"     => "",
          "Transaction_Type"   => self::TRANSACTION_PURCHASE,//Transaction Code I.E. Purchase="00" Pre-Authorization="01" etc.
          "Reference_No"       => '',
          "Customer_Ref"       => '',
          "Reference_3"        => '',
          "Client_IP"          => "",                                        //This value is only used for fraud investigation.
          "Client_Email"       => '',            //This value is only used for fraud investigation.
          "Language"           => 'en',                //English="en" French="fr"
          "Card_Number"        => $user->cardholder_data->cc_number,            //For Testing, Use Test#s VISA="4111111111111111" MasterCard="5500000000000004" etc.
          "Expiry_Date"        => $user->cardholder_data->cc_expire,            //This value should be in the format MM/YY.
          "CardHoldersName"    => $user->cardholder_data->name,
          "Track1"             => "",
          "Track2"             => "",
          "Authorization_Num"  => '',
          "Transaction_Tag"    => '',
          "DollarAmount"       => $user->transaction_data->amount,
          "VerificationStr1"   => '',
          "VerificationStr2"   => "",
          "CVD_Presence_Ind"   => "",
          "Secure_AuthRequired"=> "",
          "Currency"           => "",
          "PartialRedemption"  => ""
        );
        
        return $request;    
    }
}

class FirstDataException extends Exception{
    
    
}

class SoapClientHMAC extends SoapClient {
    
    protected $hmac;
    protected $keyid;
    protected $context;
    protected $hashtime;
    public $transaction_request;
    public $transaction_response;
    
    public function __doRequest($request, $location, $action, $version, $one_way = NULL) {
        //$hash_string = $this->calculate_hash_string( $request, $location );
        //$this->set_http_header( $request, $hash_string, $location );        
        $this->transaction_request  = $request;
        $this->transaction_response = parent::__doRequest($request, $location, $action, $version, $one_way);
        
        return $this->transaction_response; 
    }

    public function SoapClientHMAC($wsdl, $hmac, $keyid) { 
        $this->hmac    = $hmac;
        $this->keyid   = $keyid;
        $this->context = stream_context_create();
        $options['stream_context'] = $this->context;
        return parent::SoapClient($wsdl, $options);
    }
}

class FirstDataTransactionFailed extends Exception{
    public $user;
    public function __construct( user $user, $message ){
        $this->user = $user;
        parent::__construct($message);
    }
}
class FirstDataInvalidResponse   extends Exception{}
