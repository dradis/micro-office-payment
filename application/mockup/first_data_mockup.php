<?php

$status = 'success';

if( $status == 'success'){
    echo
'<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://api.globalgatewaye4.firstdata.com/vplug-in/transaction/rpc-enc/" xmlns:types="http://api.globalgatewaye4.firstdata.com/vplug-in/transaction/rpc-e..." xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
   <soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
      <q1:SendAndCommitResponse xmlns:q1="http://api.globalgatewaye4.firstdata.com/vplug-in/transaction/rpc-e...">
         <SendAndCommitResult href="#id1"></SendAndCommitResult>
      </q1:SendAndCommitResponse>
      <types:TransactionResult id="id1" xsi:type="types:TransactionResult">
         <ExactID xsi:type="xsd:string">ExactID</ExactID>
         <Password xsi:nil="true"/>
         <Transaction_Type xsi:type="xsd:string">01</Transaction_Type>
         <DollarAmount xsi:type="xsd:string">15.75</DollarAmount>
         <SurchargeAmount xsi:nil="true"/>
         <Card_Number xsi:type="xsd:string">############1111</Card_Number>
         <Transaction_Tag xsi:type="xsd:string">901975484</Transaction_Tag>
         <Track1 xsi:nil="true"/>
         <Track2 xsi:nil="true"/>
         <PAN xsi:nil="true"/>
         <Authorization_Num xsi:type="xsd:string">ET4653</Authorization_Num>
         <Expiry_Date xsi:type="xsd:string">1015</Expiry_Date>
         <CardHoldersName xsi:type="xsd:string">Donald Kerabatsos</CardHoldersName>
         <VerificationStr1 xsi:nil="true"/>
         <VerificationStr2 xsi:nil="true"/>
         <CVD_Presence_Ind xsi:type="xsd:string">0</CVD_Presence_Ind>
         <ZipCode xsi:nil="true"/>
         <Tax1Amount xsi:nil="true"/>
         <Tax1Number xsi:nil="true"/>
         <Tax2Amount xsi:nil="true"/>
         <Tax2Number xsi:nil="true"/>
         <Ecommerce_Flag xsi:type="xsd:string">0</Ecommerce_Flag>
         <XID xsi:nil="true"/>
         <CAVV xsi:nil="true"/>
         <CAVV_Algorithm xsi:nil="true"/>
         <Reference_No xsi:nil="true"/>
         <Customer_Ref xsi:nil="true"/>
         <Reference_3 xsi:nil="true"/>
         <Language xsi:nil="true"/>
         <Client_IP xsi:type="xsd:string">10.1.1.20</Client_IP>
         <Client_Email xsi:nil="true"/>
         <LogonMessage xsi:nil="true"/>
         <Error_Number xsi:type="xsd:string">0</Error_Number>
         <Error_Description xsi:nil="true"></Error_Description>
         <Transaction_Error xsi:type="xsd:boolean">false</Transaction_Error>
         <Transaction_Approved xsi:type="xsd:boolean">true</Transaction_Approved>
         <EXact_Resp_Code xsi:type="xsd:string">00</EXact_Resp_Code>
         <EXact_Message xsi:type="xsd:string">Transaction Normal</EXact_Message>
         <Bank_Resp_Code xsi:type="xsd:string">000</Bank_Resp_Code>
         <Bank_Message xsi:type="xsd:string">APPROVED</Bank_Message>
         <Bank_Resp_Code_2 xsi:nil="true"/>
         <SequenceNo xsi:type="xsd:string">025849</SequenceNo>
         <AVS xsi:nil="true"/>
         <CVV2 xsi:nil="true"/>
         <Retrieval_Ref_No xsi:type="xsd:string">08184653</Retrieval_Ref_No>
         <CAVV_Response xsi:nil="true"/>
         <MerchantName xsi:type="xsd:string">Ralphs</MerchantName>
         <MerchantAddress xsi:type="xsd:string">127 Quintana St</MerchantAddress>
         <MerchantCity xsi:type="xsd:string">Venice</MerchantCity>
         <MerchantState xsi:type="xsd:string">California</MerchantState>
         <MerchantCountry xsi:type="xsd:string">US</MerchantCountry>
         <MerchantPostal xsi:type="xsd:string">90291</MerchantPostal>
         <MerchantURL xsi:type="xsd:string">www.firstdata.com</MerchantURL>
         <CTR xsi:type="xsd:string">=========== TRANSACTION RECORD ==========
API Testing
127 Quintana St
Venice, CA 90291
US
www.firstdata.com

TYPE: Pre-Authorization

ACCT: Visa  $ 15.75 USD

CARD NUMBER : ############1111
DATE/TIME   : 18 Aug 10 09:46:52
REFERENCE # : 002 025849 M
AUTHOR. #   : ET4653
TRANS. REF. : 

    Approved - Thank You 000


Please retain this copy for your records.

Cardholder will pay above amount to card
issuer pursuant to cardholder agreement.
=========================================</CTR>
      </types:TransactionResult>
   </soap:Body>
</soap:Envelope';
} else {
    echo '';
}
