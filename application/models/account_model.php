<?php
class Account_model extends CI_Model{
    const HASH_SALT        = '238_,.\qex';
    const TBL_PERSONS      = 'cust_pers_tbl';
    const TBL_CC           = 'credit_card_tbl';
    const TBL_TRANSACTIONS = 'credit_card_txn_tbl';    
    const TBL_RECURRING    = 'payment_portal_recurring_payment_tbl';
    
    protected $key = null;
        
    public function user_has_saved_cc_details( user $user){
        $this->db->select('COUNT(*) AS EXISTS');
        $this->db->from(self::TBL_PERSONS);
        $this->db->join('credit_card_tbl', 'credit_card_tbl.person_id = cust_pers_tbl.id');  
        $this->db->where('papercut_username', $this->db->escape_str($user->username) );
        //$this->db->where('papercut_id', $this->db->escape_str($user->papercut_id) );
        $query = $this->db->get();
        return $query->row()->exists;    
    }
    
    public function authenticate( user $user ){
        $this->db->select('id, cust_acct_id, papercut_password, papercut_id, cust_acct_id');
        $this->db->from(self::TBL_PERSONS);
        $this->db->where('papercut_username', $this->db->escape_str($user->username) );
        $query = $this->db->get();
        
        $valid = false;        
        if( $query->num_rows() ){
            foreach($query->result() as $row ){
                if( $row->papercut_password   == $this->calculate_password_hash($user->password)){
                    $user->id                  = $row->id;
                    $user->account_id          = $row->cust_acct_id;
                    $user->papercut_id         = $row->papercut_id;
                    $valid = true;
                    break;
                }
            }
        }
        
        if( !$valid){
            throw new InvalidUserException($user, 'User doesnt exist');
        }
        return $user;      
    }
    
    public function user_exists_in_db( user $user){
        $this->db->select('COUNT(*) AS EXISTS');
        $this->db->from(self::TBL_PERSONS);
        $this->db->where('papercut_username', $this->db->escape_str($user->username) );
        //$this->db->where('papercut_id', (int)$user->papercut_id );
        $query = $this->db->get();
        return (bool)$query->row()->exists;        
    }
    
    public function is_user_unique(user $user){
        $this->db->select('COUNT(*) AS EXISTS');
        $this->db->from(self::TBL_PERSONS);
        $this->db->where('papercut_username', $this->db->escape_str($user->username) );
        //$this->db->where('papercut_id', (int)$user->papercut_id );
        $query = $this->db->get();
        return !(bool)$query->row()->exists;        
    }
    
    protected function get_cc_data_row_id( $user ){
        $this->db->select('id');
        $this->db->from(self::TBL_CC);
        $this->db->where('person_id', $this->db->escape_str($user->id) );
        $query = $this->db->get();
        
        $id = false;
        if( $query->num_rows() ){
            $id = $query->row()->id;
        }
        return $id;         
    }
    
    public function create_new_account(user $user){
        $data = array(
            'first_name' => $this->db->escape_str($user->first_name),
            'last_name'  => $this->db->escape_str($user->last_name),
            'papercut_username'  => $this->db->escape_str($user->username),
            'papercut_id'        => (int)$user->papercut_id,
            'papercut_password'  => $this->calculate_password_hash($user->password),
            'email'              => $this->db->escape_str($user->email)
        );
        
        $this->db->insert(self::TBL_PERSONS, $data);
    }
    
    public function save_new_account_settings(user $user){
        $data = array(
            'papercut_password' => $this->calculate_password_hash($user->password),
            'email'             => $user->email,
        );
        $this->db->where('id', $user->id );
        $this->db->update(self::TBL_PERSONS, $data);                          
    }
       
    public function save_cc_data( $user ){
        $data = array(
            'card_number'           => $this->escape_and_encrypt($user->cardholder_data->cc_number, $user->password),
            'billing_name'          => $this->escape_and_encrypt($user->cardholder_data->name,      $user->password),
            'billing_address_1'     => $this->escape_and_encrypt($user->cardholder_data->address,   $user->password),
            'billing_city'          => $this->escape_and_encrypt($user->cardholder_data->city,      $user->password),
            'billing_state'         => $this->escape_and_encrypt($user->cardholder_data->state,     $user->password),
            'billing_zip_code'      => $this->escape_and_encrypt($user->cardholder_data->zip,       $user->password),
            'billing_country'       => $this->escape_and_encrypt($user->cardholder_data->country,   $user->password),
            'card_expiration_month' => $this->escape_and_encrypt( substr($user->cardholder_data->cc_expire, 0, 2), $user->password),
            'card_expiration_year'  => $this->escape_and_encrypt( substr($user->cardholder_data->cc_expire, 2, 2), $user->password),
            'person_id'             => (int)$user->id,
            'account_id'            => (int)$user->account_id,
        );
        
        if( $cc_row_id = $this->get_cc_data_row_id($user) ){
            $this->db->where('id', $cc_row_id);
            $this->db->update(self::TBL_CC, $data);            
        }  else {
            $this->db->insert(self::TBL_CC, $data);            
        }
    }
    
    public function load_cc_data( user $user ){
        $this->db->select('card_number,billing_name,billing_address_1,billing_city,billing_state,billing_zip_code,billing_country,card_expiration_month,card_expiration_year');        
        $this->db->from(self::TBL_CC);
        $this->db->where('person_id', (int)$user->id );
        $query = $this->db->get();
        
        if($query->num_rows()){
            $user->cardholder_data->name      = $this->decrypt_data( $query->row()->billing_name, $user->password );
            $user->cardholder_data->address   = $this->decrypt_data( $query->row()->billing_address_1, $user->password );
            $user->cardholder_data->city      = $this->decrypt_data( $query->row()->billing_city, $user->password );
            $user->cardholder_data->state     = $this->decrypt_data( $query->row()->billing_state, $user->password );
            $user->cardholder_data->zip       = $this->decrypt_data( $query->row()->billing_zip_code, $user->password );
            $user->cardholder_data->country   = $this->decrypt_data( $query->row()->billing_country, $user->password );
            $user->cardholder_data->cc_expire = $this->decrypt_data( $query->row()->card_expiration_month, $user->password ).$this->decrypt_data( $query->row()->card_expiration_year, $user->password );
            $user->cardholder_data->cc_number = $this->decrypt_data( $query->row()->card_number, $user->password);
            $user->is_cc_data_saved = true;       
        } else {
            throw new InvalidUserDataException($user, 'Invalid cc data stored');        
        }
        
        return $user;      
    }
    
    public function delete_cc_data(user $user){
        if( $user->id ){
            $this->db->delete(self::TBL_CC, array('person_id' => $user->id));    
        }         
    }
    
    public function load_transactios( user $user ){
        $this->db->select('billing_name,billing_address_1,billing_city,billing_state,billing_zip_code,billing_country,response_avs,response_error,response_approved,response_code,response_message,total_charge, queued_dt, notes');
        $this->db->from(self::TBL_TRANSACTIONS);
        $this->db->where('person_id', (int)$user->id );        
        $query = $this->db->get();
        
        $transactions = array();
        if( $query->num_rows() ){
            foreach($query->result() as $row ){
                $transactions[] = $row;
            }           
        }
        return $transactions; 
    }
    
    public function log_transactions( user $user ){
        $data = array(
            'card_number'           => 'NA',
            'billing_name'          => $this->db->escape_str($user->cardholder_data->name),
            'billing_address_1'     => $this->db->escape_str($user->cardholder_data->address),
            'billing_city'          => $this->db->escape_str($user->cardholder_data->city),
            'billing_state'         => $this->db->escape_str($user->cardholder_data->state),
            'billing_zip_code'      => $this->db->escape_str($user->cardholder_data->zip),
            'billing_country'       => $this->db->escape_str($user->cardholder_data->country),
            'card_expiration_month' => $this->db->escape_str('NA'),
            'card_expiration_year'  => $this->db->escape_str('NA'),
            'person_id'             => (int)$user->id,
            'response_avs'          => $this->db->escape_str($user->transaction_data->AVS),
            'response_error'        => $this->db->escape_str($user->transaction_data->error_message),
            'response_approved'     => $this->db->escape_str($user->transaction_data->Authorization_Num),
            'response_code'         => $this->db->escape_str($user->transaction_data->error_code),
            'response_message'      => $this->db->escape_str($user->transaction_data->error_message),
            'total_charge'          => (float)$user->transaction_data->amount,
            'notes'                 => $this->db->escape_str($user->transaction_data->description )
        );        
        
        $this->db->insert(self::TBL_TRANSACTIONS, $data);        
    }
    
    public function get_recurring_payment_data( $user ){
        $this->db->select('balance_limit, balance_recharge');
        $this->db->from(self::TBL_RECURRING);
        $this->db->where('person_id', $this->db->escape_str($user->id));
        $query = $this->db->get();
        if( $query->num_rows()){
            $user->recurring_payment_data->balance_limit    = $query->row()->balance_limit;
            $user->recurring_payment_data->balance_recharge = $query->row()->balance_recharge;
            $user->has_recurring_payment = true;
        }
        return $user;    
    }
           
    public function delete_recurring_payment( $user ){
        if( $user->id ){
            $this->db->delete(self::TBL_RECURRING, array('person_id' => $user->id));    
        }      
    }
    
    public function save_recurring_payment( $user ){
        $data = array(
            'person_id'             => (int)$user->id,
            'balance_limit'         => (float)$user->recurring_payment_data->balance_limit,
            'balance_recharge'      => (float)$user->recurring_payment_data->balance_recharge,
            'cc_password'           => $this->escape_and_encrypt($user->password, $this->config->item('ccpass_key'))
        );
        
        if( $row_id = $this->get_recurring_payment_data_row_id($user) ){
            $this->db->where('id', $row_id);
            $this->db->update(self::TBL_RECURRING, $data);            
        }  else {
            $this->db->insert(self::TBL_RECURRING, $data);            
        }
    }
    
    protected function get_recurring_payment_data_row_id( $user ){
        $this->db->select('id');
        $this->db->from(self::TBL_RECURRING);
        $this->db->where('person_id', $this->db->escape_str($user->id) );
        $query = $this->db->get();
        
        $id = false;
        if( $query->num_rows() ){
            $id = $query->row()->id;
        }
        return $id;         
    }    
    
    public function user_has_recurring_payment_data($user){
        $has = false;
        if($this->get_recurring_payment_data_row_id($user) !== false){
            $has = true;
        }
        return $has;
    }
           
    protected function escape_and_encrypt( $data, $pass ){
        $key = $this->get_key($pass);
        return $this->db->escape_str($this->encrypt->encode($data, $key));    
    }
    
    protected function decrypt_data( $data, $pass ){
        $key = $this->get_key($pass);
        return $this->encrypt->decode($data, $key);
    }
    
    protected function get_key($pass){
        if(!isset($this->key[$pass]) || !$this->key[$pass]){
            $this->key[$pass] = $this->calculate_password_hash( md5($this->config->item('encryption_key').$pass));
        }
        return $this->key[$pass];
    }
    
    protected function calculate_password_hash($pass){
        return md5(self::HASH_SALT.$pass); 
    }
}

abstract class userdata{
    protected $required = array();
    public $errors = array();
    public function verify(){
        $valid = true;
        foreach( $this->required as $field=>$pattern ){
            if( $this->$field === null || !preg_match( $pattern, $this->$field )){
                $this->errors[$field] = true;
                $valid = false;
            }
        }
        return $valid;
    }
}

class user extends userdata{
    protected $required = array(
        'password'   => '!^.+$!',
        'email'      => '!@!',
        'first_name' => '![a-zA-Z]!',
        'last_name'  => '![a-zA-Z]!',
    );    
    public $email;
    public $username;
    public $first_name;
    public $last_name;
    public $papercut_id;
    public $account_id;
    public $id;
    public $password;
    public $cardholder_data;
    public $transaction_data;
    public $balance;
    public $is_cc_data_saved = false;
    public $is_logged        = false;
    public $is_registered    = false;
    public $has_recurring_payment = false;
    
    public function __construct(){
        $this->cardholder_data  = new cardholder_data();
        $this->transaction_data = new transaction_data();
        $this->recurring_payment_data = new recurring_payment_data();
    }
    
    public function is_valid(){
        if( $this->username){
            return true;
        }
        return false;
    }
    
    public function verify_payment_gateway_data(){
        $this->cardholder_data->verify();
        $this->transaction_data->verify();
        if( $this->cardholder_data->errors || $this->transaction_data->errors ){
            throw new InvalidUserDataException($this, 'Invalid credit card data');
        }
    }
    
    public function verify_persons_tbl_record_data(){
         
        if(!$this->verify()){
            throw new InvalidPersonsTblRecordException($this, 'Invalid user data');
        }
    }
    
    public function verify_recurring_payment_data(){
        if(!$this->recurring_payment_data->verify()){
            throw new InvalidUserDataException($this, 'Invalid recurring payment data');
        }
    }    
    
    public function set_error($field, $value){
        $this->errors[$field] = $value;
    }
    
    public function same_as( user $user){
        $same = true;
        if( $this->username != $user->username ){
            $same = false;
        }
        return $same;
    }
}

class cardholder_data extends userdata{
    protected $required = array(
        'cc_number' => '!^\d{10,16}$!',
        'cc_cvn'    => '!^\d{1,4}$!',
        'cc_expire' => '!\d+!',
        'name'      => '![a-zA-Z]!',
    );
    public $name;
    public $address;
    public $city;
    public $zip;
    public $state;
    public $country;
    public $cc_number;
    public $cc_cvn;
    public $cc_expire;
    
    public function get_month(){
        if( $this->cc_expire ){
            return substr( $this->cc_expire, 0, 2 );
        }
        return '';
    }
    
    public function get_year(){
        if( $this->cc_expire ){
            return substr( $this->cc_expire, 2, 2);
        }
        return ''; 
    }
    
    public function get_months(){
        return array(
          ''   => '-Month',
          '01' => '01',
          '02' => '02',
          '03' => '03',
          '04' => '04',
          '05' => '05',
          '06' => '06',
          '07' => '07',
          '08' => '08',
          '09' => '09',
          '10' => '10',
          '11' => '11',
          '12' => '12',
        );
    }  
      
    public function get_years(){
        $year  = gmdate('Y');
        $array = array( '' => '-Year');
        
        $limit = $this->get_year() && ($this->get_year() + 2000) > $year ? ($this->get_year() + 2000) - $year : 10; 
        for( $i = 1; $i<=$limit; $i++ ){
            $year++;
            $array[ $year - 2000 ] = $year;
        }
        return $array; 
    }    
}

class transaction_data extends userdata{
    protected $required = array(
        'amount' => '!\d+!'
    );
    public $amount;
    public $currency;
    public $description;
    public $xml_request;
    public $xml_response;
    public $AVS;
    public $error_message;
    public $Authorization_Num;
    public $error_code;

}

class recurring_payment_data extends userdata{
    public $supported_values = array('10','25','50');
    public $balance_limit;
    public $balance_recharge;
    
    public function verify(){
        $valid = true;
        if( !in_array($this->balance_limit, $this->supported_values ) || 
            !in_array($this->balance_recharge, $this->supported_values )
        ){
            $valid = false;
        }
        return $valid;
    }
}

class InvalidUserDataException         extends PaymentPortalException{}
class InvalidUserException             extends PaymentPortalException{}
class InvalidCredentialsException      extends PaymentPortalException{}
class InvalidPersonsTblRecordException extends PaymentPortalException{}