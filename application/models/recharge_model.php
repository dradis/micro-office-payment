<?php
class recharge_model extends Account_model{
    const TBL_RECURRING_PAYMENTS = '';
    const TBL_CRON_LOGS          = '';
    
    public function log_cron_activity(cron_activity $activity){
        //Stub in case we want to log cron activity    
    }
    
    public function get_payment_portal_users_with_recurring_payments(){
        $this->db->select('person_id, balance_limit, balance_recharge, papercut_username, cc_password, papercut_id');
        $this->db->from(self::TBL_RECURRING);
        $this->db->join(self::TBL_PERSONS, 'cust_pers_tbl.id = payment_portal_recurring_payment_tbl.person_id');  
        $query = $this->db->get();
        $data = array();
        if( $query->num_rows()){
            foreach($query->result() as $row){
                $user = new user();
                $user->id                                       = $row->person_id;
                $user->papercut_id                              = $row->papercut_id;
                $user->recurring_payment_data->balance_limit    = $row->balance_limit;
                $user->recurring_payment_data->balance_recharge = $row->balance_recharge;
                $user->username                                 = $row->papercut_username;
                $user->password = $this->decrypt_data($row->cc_password, $this->config->item('ccpass_key'));
                $user = $this->load_cc_data($user);
                $data[] = $user;
            }
        }
        return $data;
    }
    
}

class cron_activity{
    public $start;
    public $end;
    public $accounts_recharged;
    public $total_amount_recharged;
}
