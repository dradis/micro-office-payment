Small payment portal for odesk freelance project. PHP/Codeigniter/PostgreSQL.

Custom (non framework) code is in 
application/controllers,
application/models,
application/libraries,
application/views,